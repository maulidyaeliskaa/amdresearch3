import JudulResearch from "@/app/judulResearch";
import Progress from "./Progress";

export default function ResearchDetailManager() {
  return (
    <>
      <JudulResearch />
      <div id="ResearchDetails">
        <div id="ResearchDetailsLeft">
          <p>Latar Belakang Research</p>
          <p>
            Sosial media telah menjadi salah satu sumber utama informasi di
            masyarakat. Diperlukan penelitian untuk mengetahui hubungan media
            sosial dengan pengaruh politik.
          </p>
          <p>Kategori Research</p>
          <p>Sosial</p>
          <p>GoalsResearch</p>
          <p>Mengetahui pengaruh media sosial terhadap opini politik publik</p>
        </div>
        <Progress />
      </div>
      <div id="ResearchDetailsButton">
        <button>Assign a Researcher</button>
      </div>
    </>
  );
}
