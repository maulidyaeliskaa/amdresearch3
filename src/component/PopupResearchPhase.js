export default function PopUpResearchPhase({ isPopupVisible, onClose }) {
  if (!isPopupVisible) return null;
  return (
    <div id="PopUp">
      <div id="close" onClick={onClose}>
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect width="24" height="24" rx="12" fill="#E0E0E0" />
          <path
            d="M16 8L8 16M8 8L16 16"
            stroke="#545559"
            strokeWidth="1.74603"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      </div>

      <div className="flex" id="penjelasan-popup">
        <div>
          <svg
            width="56"
            height="56"
            viewBox="0 0 66 66"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect x="5" y="5" width="56" height="56" rx="28" fill="#D5FBD4" />
            <path
              d="M34.1667 21.3333L23.7757 33.8024C23.3688 34.2907 23.1653 34.5349 23.1622 34.7411C23.1595 34.9204 23.2394 35.0909 23.3788 35.2036C23.5392 35.3333 23.857 35.3333 24.4927 35.3333H33L31.8333 44.6666L42.2243 32.1974C42.6312 31.7091 42.8347 31.4649 42.8378 31.2587C42.8405 31.0794 42.7606 30.9089 42.6212 30.7962C42.4608 30.6666 42.143 30.6666 41.5073 30.6666H33L34.1667 21.3333Z"
              stroke="#00C75D"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
            <rect
              x="5"
              y="5"
              width="56"
              height="56"
              rx="28"
              stroke="#E8FDED"
              strokeWidth="10"
            />
          </svg>
        </div>
        <div id="text-popup">
          <h2>Define Phase Completed!</h2>
          <p>
            Researcher akan mendapatkan notifikasi terkait fase yang telah
            disetujui. Anda akan mendapat notifikasi terkait tahap selanjutnya.
          </p>
        </div>
      </div>
    </div>
  );
}
