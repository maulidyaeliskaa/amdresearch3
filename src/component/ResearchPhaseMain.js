import ResearchPhaseCard from './ResearchPhaseCard';

export default function ResearchPhaseMain({ onResearchPhaseDetailClick, dataResearch, isLoading }) {
  if (isLoading) return <p>Loading ...</p>;

  if (dataResearch.message) return <p>{dataResearch.message}</p>;

  return (
    <>
      <div id="ResearchPhaseMain">
        {dataResearch.map((research) => (
          <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick} key={research.id} research={research} />
        ))}
        {/* <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick} />
        <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick} />
        <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick} />
        <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick} />
        <ResearchPhaseCard onResearchPhaseDetailClick={onResearchPhaseDetailClick} /> */}
      </div>
    </>
  );
}
