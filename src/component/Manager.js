import React, { useEffect, useState } from 'react';
import Popup from './PopUp';
import api from '../api/api';

export default function Manager({ onInitiateResearchDetailClick, fields, setFields, handleSubmit }) {
  const [isPopupVisible, setIsPopupVisible] = useState(false);
  const [researchManagers, setResearchManagers] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  async function getListManager() {
    setIsLoading(true);
    try {
      const response = await api.get('list-research-manager');
      const result = response.data.data;
      setResearchManagers(result);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getListManager();
  }, []);

  function popup() {
    setIsPopupVisible(!isPopupVisible);
  }

  function onClose() {
    setIsPopupVisible(false);
    window.location.href = '/Dashboard';
  }

  function asigned(managerId) {
    const bg = document.getElementById('rectSVG');
    const plus = document.getElementById('pathSVG');

    bg.classList.toggle('AsignedBackground');
    plus.classList.toggle('AsignedPlus');

    setFields((prev) => ({
      ...prev,
      assign_research: managerId,
    }));
  }

  function asigned1() {
    const bg = document.getElementById('rectSVG1');
    const plus = document.getElementById('pathSVG1');

    bg.classList.toggle('AsignedBackground');
    plus.classList.toggle('AsignedPlus');
  }

  if (isLoading) return <p>Loading ....</p>;

  if (!researchManagers) return <div>No data</div>;

  return (
    <section>
      <div id="jxBRWC4sO7">
        <p> Assign to Research Manager</p>
      </div>

      {researchManagers.map((manager) => (
        <div id="tabel-list" key={manager.id}>
          <img src="/assets/images/manager 1.png" width="60" height="60"></img>
          <div id="list">
            <p> {manager.nama} </p>
            <p> Jakarta </p>
          </div>
          <div id="list-data">
            <p> </p>
            <p> </p>
          </div>
          <div id="list-data">
            <p> </p>
            <p> </p>
          </div>
          <div id="list-jabatan">
            <p> </p>
            <p> </p>
          </div>
          <div id="list-data">
            <p> </p>
            <p> </p>
          </div>
          <div id="tambah-manager" onClick={() => asigned(manager.id)}>
            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect id="rectSVG" x="1" y="1" width="24" height="24" rx="12" fill="white" />
              <path
                id="pathSVG"
                d="M13 8.33331V17.6666M8.33331 13H17.6666"
                stroke="#A3A3A3"
                strokeWidth="1.33333"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <rect
                x="1"
                y="1"
                width="24"
                height="24"
                rx="12"
                stroke="#E0E0E0"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-dasharray="1 3"
              />
            </svg>
          </div>
        </div>
      ))}

      <div id="button-manager">
        <button onClick={onInitiateResearchDetailClick}>Cancel</button>
        <button onClick={popup}> Submit </button>
      </div>
      {isPopupVisible && <Popup isPopupVisible={isPopupVisible} onClose={onClose} />}
    </section>
  );
}
