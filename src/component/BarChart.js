import { Line } from "react-chartjs-2";
import {
    Chart as ChartJS,
    BarElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Legend,
    Filler
} from 'chart.js';
import { Bar } from "react-chartjs-2";

ChartJS.register(
    BarElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Legend,
    Filler
)

export default function BarChart(){

    const pointImageRed = new Image();
    pointImageRed.src = '/assets/pointStyle/red.svg';
    const pointImageOrange = new Image();
    pointImageOrange.src = '/assets/pointStyle/orange.svg';
    const pointImageGreen = new Image();
    pointImageGreen.src = '/assets/pointStyle/green.svg';
    const pointImageBlue = new Image();
    pointImageBlue.src = '/assets/pointStyle/blue.svg';

    const data = {
        labels: ['Mon', 'Tue', 'Wed'],
        datasets: [
            {
            label: 'Instagram',
            data: [1, 5, 9],
            backgroundColor: 'rgba(67, 57, 242, 1)',
            borderColor: 'blue',
            pointBorderColor: 'blue',
            borderRadius: Number.MAX_VALUE,
            fill: true,
            pointRadius: 0,
            tension: 0.4,
            pointStyle: [pointImageBlue, '', '', pointImageBlue],
            borderSkipped: false
        },
            {
            label: 'Facebook',
            data: [2, 6, 10],
            backgroundColor: 'rgba(52, 181, 58, 1)',
            borderColor: 'green',
            pointBorderColor: 'green',
            borderRadius: Number.MAX_VALUE,
            fill: true,
            pointRadius: 0,
            tension: 0.4,
            pointStyle: [pointImageGreen, '', '', pointImageGreen],
            borderSkipped: false
        },
            {
            label: 'twitter',
            data: [3, 7, 11],
            backgroundColor: 'rgba(255, 178, 0, 1)',
            borderColor: 'orange',
            pointBorderColor: 'orange',
            borderRadius: Number.MAX_VALUE,
            fill: true,
            pointRadius: 0,
            tension: 0.4,
            pointStyle: [pointImageOrange, '', '', pointImageOrange],
            borderSkipped: false
        },
    ]
    }

    const config = {
        type: 'bar'
    }

    const options = {
        plugins: {
            title: {
                display: true,
                text: 'Line Diagram',
            },
            legend: {
                labels: {
                    usePointStyle: true,
                }
            }
        },
        barPercentage: 0.0923,
        scales: {
            y: {
                display: true,
                min: 0,
                max: 12,
                grid: {
                    display: false,
                },
                border: {
                    display: false
                }
            },
            x:{
                display: true,
                stacked: true,
                grid: {
                    display: false
                },
                border: {
                    display: false
                }
            }
        }
    }

    return(
        <div id="BarDiagram">
            <p>Bar Chart</p>
            <Bar
                data={data}
                options={options}
                config={config}
            ></Bar>
        
        </div>
    )
}