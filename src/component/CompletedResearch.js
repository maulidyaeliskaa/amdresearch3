import { useEffect, useState } from 'react';
import api from '../api/api';

export default function CompletedResearch() {
  const [listResearch, setListResearch] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  async function getListResearch() {
    setIsLoading(true);
    try {
      const response = await api.get('/get-research');
      const result = response.data.data;
      const filteringCompletedResearch = await result.filter((research) => research.phase >= 3);
      setListResearch(filteringCompletedResearch);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getListResearch();
  }, []);

  if (isLoading) return <p>Loading ....</p>;

  return (
    <div className="wrapper">
      {listResearch?.length !== 0 && (
        <>
          <div className="button-wrapper">
            <button className="button-more">
              More <img src="/assets/images/right-arrow.png"></img>
            </button>
          </div>
          <h2>Completed Research</h2>

          <ul className="carousel">
            {listResearch?.map((research) => (
              <li key={research.id} className="card">
                <div>
                  <img src="/assets/images/Image.png"></img>
                  <h3>{research.title_research}</h3>
                  <p>{research.background_research}</p>
                  <img className="photo" src="/assets/images/Photo3.png"></img>
                  <img className="photo" src="/assets/images/Photo2.png"></img>
                </div>
              </li>
            ))}
          </ul>
        </>
      )}
    </div>
  );
}
