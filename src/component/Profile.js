import Cookies from "js-cookie"
import { useNavigate } from "react-router-dom"

export default function Profile(){

    const navigate = useNavigate();

    function handleLogout(){
        Cookies.remove('token');
        navigate("/");
    }

    return(
        <div id="ProfileDropDown" className="dropDown">
            <a href="#">My Profile</a>
            <a href="#" onClick={handleLogout}>Logout</a>
        </div>
    )
}