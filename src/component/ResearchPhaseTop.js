export default function ResearchPhaseTop({ researchPhase, handleClickPhase, selectedPhase }) {
  return (
    <div id="ResearchPhaseTop">
      {/* <div id="ResearchPhaseTopCard">
        <p>Define</p>
        <div>
          <p>6</p>
          <p>Research</p>
        </div>
      </div>
      */}

      {researchPhase?.map((item, index) => (
        <div
          onClick={() => handleClickPhase(index)}
          key={item.phase_name}
          id={`${selectedPhase === index ? 'ResearchPhaseTopCard' : 'ResearchPhaseTopCardSecondary'}`}
          style={{ cursor: 'pointer' }}
        >
          <p>{item.phase_name}</p>
          <div>
            <p>{item.phase_value}</p>
            <p>Research</p>
          </div>
        </div>
      ))}
    </div>
  );
}
