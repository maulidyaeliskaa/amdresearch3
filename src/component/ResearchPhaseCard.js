export default function ResearchPhaseCard({ onResearchPhaseDetailClick, research }) {
  const { title_research, background_research, researcher } = research;

  return (
    <>
      <div id="ResearchPhaseCard" onClick={onResearchPhaseDetailClick}>
        <img src="/assets/images/Image.png" alt="" width={326} height={160} />
        <div>
          <p>{title_research}</p>
          <p>{background_research}</p>
          <div>
            {researcher.map((item, index) => (
              <img key={index} src="/assets/images/Photo.jpg" alt="" width={40} height={40} />
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
